#include "comms.h"
#include <RFM69.h>
#include <SPI.h>


#include <RFM69.h>


//#define NODEID        2
//#define NETWORKID     100
//#define GATEWAYID     1
//
//RFM69 radio;
//void setup() {
//	// put your setup code here, to run once:
//	Serial.begin(115200);
//
//	if (radio.initialize(FREQUENCY, NODEID, NETWORKID))
//		Serial.println("OK");
//	radio.setHighPower();
//}
//
//void loop() {
//	// put your main code here, to run repeatedly:
//
//}

CommsRF::CommsRF(u8 node, u8 network, const char* encryptKey)
    : m_radio(nullptr)
    , m_sendList(nullptr)
{
    m_radio = new RFM69();
    if (m_radio->initialize(RF69_433MHZ, node, network))
        Serial.println("RADIO OK!!!!"); // TODO logger

    m_radio->setHighPower();

    //if (encryptKey)
    //	m_radio->encrypt(encryptKey);

    m_sendList = new Buffer();
}

CommsRF::~CommsRF()
{
    if (m_radio)
        delete m_radio;
}

CommsRF::Buffer::Buffer()
    : id(INVALID_BUFFER_ID)
    , next(nullptr)
    , retries(0)
{
}

CommsRF::Buffer::~Buffer()
{
    delete[] data; 
}

BufferId CommsRF::Buffer::s_nextId = 0;

// TODO buffering 
BufferId CommsRF::Send(const void* buffer, u8 bufferSz, u8 targetNode, bool requestACK, MsgType type, bool overwrite)
{
    if (overwrite)
    {
        Buffer* node = m_sendList->next;
        while (node)
        {
            if (node->type == type)
                node = RemoveSendBuffer(node->id);
            else
                node = node->next;
        }
    }

    Buffer* topNode = m_sendList;
    while (topNode->next) topNode = topNode->next;
    topNode->next = new Buffer;
    
    Buffer* newBuf = topNode->next;
    newBuf->data = new u8[bufferSz];
    memcpy(newBuf->data, buffer, bufferSz);
    newBuf->dataSz = bufferSz;
    newBuf->id = Buffer::s_nextId++;
    newBuf->targetNode = targetNode;
    newBuf->type = type;
    return newBuf->id;
}

CommsRF::Buffer* CommsRF::RemoveSendBuffer(BufferId id)
{
    Buffer* target = m_sendList;
    while (target)
    {
        if (target->id == id)
            break;
        target = target->next;
    }

    Buffer* next = nullptr;
    if(target)
    {
        next = target->next;
        delete target;
    }
    return next;
}

// TODO do this properly 
void CommsRF::Tick()
{
    Buffer* buffer = m_sendList->next;
    while (buffer && buffer->id != INVALID_BUFFER_ID)
    {
        static const bool requestACK = true;
        if (requestACK)
        {
            if (m_radio->sendWithRetry(buffer->targetNode, buffer->data, buffer->dataSz)) // TODO handle ACK not received (e.g. copy buffer, try later, overwrite if new data is to be sent)
            {
                buffer = RemoveSendBuffer(buffer->id);
                Serial.println("Received ACK");
            }
            else
            {
                buffer->retries++;
                // TODO temp
                Serial.print("No ACK received; retries: "); Serial.println(buffer->retries);
                if (buffer->retries > 10) buffer = RemoveSendBuffer(buffer->id);
                else buffer = buffer->next;
            }
        }
        else
        {
            m_radio->send(buffer->targetNode, buffer->data, buffer->dataSz);
            buffer = RemoveSendBuffer(buffer->id);
        }
    }

    if (!m_radio->receiveDone())
        return;

    memcpy(m_data, m_radio->DATA, m_radio->DATALEN);
    m_dataSz = m_radio->DATALEN;

    if (m_radio->ACKRequested())
        m_radio->sendACK();
}

void CommsUART::Tick() 
{
    /*
    Command newCmd;
    Serial.readBytes(newCmd.bytes, 4); // !! BLOCKING !!
    if(fabs(newCmd.angle - m_target) > FLT_EPSILON)
    {
    m_target = newCmd.angle;
    Serial.print("REMOTE COMMAND: "); Serial.println(newCmd.angle);
    }

    /*
    while (Serial.available() > 0)
    buffer.AddByte(Serial.read());

    if (buffer.GetNextCommand(newCmd))
    {
    if (fabs(newCmd.angle - m_target) > FLT_EPSILON)
    {
    m_target = newCmd.angle;
    Serial.println(newCmd.angle);
    }
    }
    */
}

BufferId CommsUART::Send(const void* buffer, u8 bufferSz, u8 target, bool requestACK, MsgType type, bool overwrite) 
{
    /*if (m_sendCommand)
    {
    if (inputType == VALUE) // this is AFTER the switch, hence "real" type is VALUE
    m_inputAngle = presetAngles[m_inputPreset];

    union
    {
    float angle;
    u8 bytes[4];
    } cmd;
    cmd.angle = m_inputAngle;

    Serial.write(cmd.bytes, 4); // Kills btns 1, 2, 3, 4
    //Serial.flush();
    m_sendCommand = false;
    }*/
    return INVALID_BUFFER_ID;
}