#pragma once
#include "system.hpp"
#include "third_party/Adafruit_LCD/Adafruit_LCD.h"


// Dependencies 
class Sensing;
//class InputManager;
#include "input_manager.hpp"
#include "ui_feedback.hpp"

struct ActuationCmd
{
    ActuationCmd();
  
    float GetTargetAngle() const;
  
private:  
    friend class UserInterface;
    float m_targetAngle;    
};

class UserInterface : public System
{
    // Only one instance allowed at any one time
    static bool s_instantiated;
    
public:
    UserInterface(const Sensing* const, const InputManager* const, const ActuationFeedback* const, const SensingFeedback* const);
    ~UserInterface();
    UserInterface(const UserInterface&) = delete;
    UserInterface& operator=(const UserInterface&) = delete;
    
    void Tick() override;
    
    const ActuationCmd* const GetActuationCmdRef() const;
    
private:
    enum Screen
    {
        INIT,
        OPERATION
    } m_curScreen = OPERATION;
    
    void TickInit();
    void TickOperation();

    ActuationCmd m_actuationCmd;
    Adafruit_LiquidCrystal m_display;
    
    const Sensing*      const m_sensing;
    const InputManager* const m_inputMgr;
    const ActuationFeedback* const m_actFeedback;
    const SensingFeedback* const m_senseFeedback;
    
    static const u8 s_numCols = 20;
    static const u8 s_numRows = 4;
    
    void ClearLine(int line, int offset = 0);
    
    enum InputType
    {
        PRESET = 0,
        VALUE
    } inputType;

    enum class FlapPreset : int
    {
        LANDING = 0,
        TAKEOFF,
        MIN,
        MAX,
        NEUTRAL,

        NONE
    };

    const float presetAngles[int(FlapPreset::NONE)] = {
        10.4f,    // Landing
        5.9f,     // Takeoff
        -10.f,    // Min
        30.f,     // Max
        0.f       // Neutral
    };

    const char flapPresetNames[int(FlapPreset::NONE) + 1][10] = {
        "Landing",
        "Takeoff",
        "Minimum",
        "Maximum",
        "Neutral",
        "!ERROR!"     // Should never display a non-existing preset
    };

    float m_inputAngle = 0.f;
    /*FlapPreset*/int m_inputPreset = int(FlapPreset::NEUTRAL);
    
    unsigned long m_dt, m_lastTick;

    int FindPreset(float angle);

    bool m_refereshScreen = true;
    
    void ClearInput();
    void CommitCommand();
    void SwitchType();
    
    void OnKeyUpPreset(Keys);
    void OnKeyUpValue(Keys);
    
    void OnKeyHoldValue(Keys, u32 dt);
};
