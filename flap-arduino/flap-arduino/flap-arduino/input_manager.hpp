#pragma once
#include "system.hpp"
#include "third_party/Keypad/Keypad.h"


// Hardcoded to a single physical configuration:
//      Keypad
//                4x4 matrix 
//             A1  10   8   9
//             ^    ^   ^   ^
//             |    |   |   |
//             |    |   |   |
//             |    |   |   |
//            #################
//            # 7 # 8 # 9 # > # -----> A2
//            # 4 # 5 # 6 # < # -----> A3
//            # 1 # 2 # 3 # I # -----> A4
//            # C # 0 # E # . # -----> A5
//            #################
//
enum Keys : char 
{
    NONE = 0,
    
    KEY_7 = 7,  KEY_8 = 8, KEY_9 = 9,  RIGHT = 13,
    KEY_4 = 4,  KEY_5 = 5, KEY_6 = 6,  LEFT = 14,
    KEY_1 = 1,  KEY_2 = 2, KEY_3 = 3,  TYPE = 15,
    CLEAR = 16, KEY_0 = 0, ENTER = 17, DOT = 18 
};

class InputManager : public System
{
    // Only one instance allowed at any one time
    static bool s_instantiated;  
    
public:
    InputManager();
    ~InputManager();
    InputManager(const InputManager&) = delete;
    InputManager& operator=(const InputManager&) = delete;
    
    void Tick() override;      
    
    // Current key being held down
    Keys GetKeyDown() const;
    
    // Last key clicked
    // Will invalidate key after every call 
    Keys GetLastKeyUp();
    
private:      
    Keypad m_keypad;
    
    // Continuous input
    Keys   m_keyDown; 
    u32    m_keyDownTimer;
    const 
    u32    s_keyDownTimerLimit = 100;
    
    // Discrete clicks 
    Keys m_lastKeyUp;
    bool m_poppedUp;
};
