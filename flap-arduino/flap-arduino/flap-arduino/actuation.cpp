#include "actuation.hpp"
#include <assert.h>
#include "sensing.hpp"
#include "user_interface.hpp"


enum class MotorPins : u8
{
    DIR = 3,
    STEP = 4,
    ENABLE = 6
};

bool Actuation::s_instantiated = false;

Actuation::Actuation(const Sensing* const sensingRef, const ActuationCmd* const uiCmdRef, ActuationFeedback* const uiFeedback)
    : m_sensing(sensingRef)
    , m_command(uiCmdRef)
    , m_uiFeedback(uiFeedback)
    , m_motorState(MotorState::IDLE)
{
    assert(!s_instantiated);
    s_instantiated = true;
    
    assert(m_sensing);
    assert(m_command);
    
    pinMode(u8(MotorPins::DIR), OUTPUT);
    pinMode(u8(MotorPins::STEP), OUTPUT);
    pinMode(u8(MotorPins::ENABLE), OUTPUT);
    
    digitalWrite(u8(MotorPins::ENABLE), LOW);
}

Actuation::~Actuation()
{
    s_instantiated = false;
}

void Actuation::Tick()
{
    if (!m_sensing->IsStable())
        return;
    
    const float estDeflection = m_sensing->GetEstimatedDeflection();
    const float target = m_command->GetTargetAngle();
    
    // Extra step insted of going straight to actuation meant for feedback on the actuation system's state to the
    // user interface. Since this system already has a dependency on the ui system, this might be removed to avoid
    // a dependecy the other way around
    m_motorState = MotorState::IDLE;
    m_uiFeedback->m_target = -1000.f;
    
    // Is action needed?
    if (fabs(estDeflection - target) > m_errorBound && target > -100.f)
    {
        m_uiFeedback->m_target = target;
        if (estDeflection < target) 
            m_motorState = MotorState::MOVE_CCW;    
        else
            m_motorState = MotorState::MOVE_CW;
    }
    
    if (m_motorState == MotorState::MOVE_CW)  digitalWrite(u8(MotorPins::DIR), HIGH);
    else if (m_motorState == MotorState::MOVE_CCW) digitalWrite(u8(MotorPins::DIR), LOW);
    else return;
    
    digitalWrite(u8(MotorPins::ENABLE), HIGH);
    
    const int steps = 1;
    const int speed = 5000; //8000
    for (int i = 0; i < steps; ++i) 
    {
        digitalWrite(u8(MotorPins::STEP), HIGH);
        delayMicroseconds(speed);
        digitalWrite(u8(MotorPins::STEP), LOW);
        delayMicroseconds(speed);
    }
    
    digitalWrite(u8(MotorPins::ENABLE), LOW);
}