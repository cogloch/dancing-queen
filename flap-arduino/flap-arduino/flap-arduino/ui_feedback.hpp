#pragma once


struct ActuationFeedback
{
    // If less than -100, holding
    float GetTarget() const { return m_target; }
    
private:
    friend class Actuation;
    float m_target;     
};

struct SensingFeedback
{
    float GetDeflectionEstimate() const { return m_deflection; }
    
private:
    friend class Sensing;
    float m_deflection;
};
