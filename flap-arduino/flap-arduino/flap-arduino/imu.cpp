#include "imu.hpp"
#include <Arduino.h>


void IMU::StoreState(unsigned long time, const Vec3f& angles, const Vec3f& gyroAngles) 
{
    last_read_time = time;
    lastAngles = angles;
    lastGyroAngles = gyroAngles;
}

void IMU::Tick()
{
    IMUPack sensorData;

    // Read the raw values.
    GetRawData((u8*)&sensorData);

    // Get the time of reading for rotation computations
    unsigned long tickStart = millis();

    // Get raw acceleration values
    Vec3f accel = {
        sensorData.value.x_accel,
        sensorData.value.y_accel,
        sensorData.value.z_accel
    };

    // Get angle values from accelerometer
    float RADIANS_TO_DEGREES = 180 / 3.14159;
    Vec3f accelAngle = {
        atan(accel.y / sqrt(pow(accel.x, 2) + pow(accel.z, 2)))*RADIANS_TO_DEGREES,
        atan(-1 * accel.x / sqrt(pow(accel.y, 2) + pow(accel.z, 2)))*RADIANS_TO_DEGREES,
        0
    };

    // Convert gyro values to deg/s
    float FS_SEL = 131;
    Vec3f gyro = {
        (sensorData.value.x_gyro - baseGyr.x) / FS_SEL,
        (sensorData.value.y_gyro - baseGyr.y) / FS_SEL,
        (sensorData.value.z_gyro - baseGyr.z) / FS_SEL
    };

    // Filtered gyro angles 
    float dt = (tickStart - last_read_time) / 1000.0;
    Vec3f filteredGyro = gyro * dt + lastAngles;

    // Drifting gyro angles
    Vec3f unfilteredGyro = gyro * dt + lastGyroAngles;

    // Complementary filter
    float alpha = 0.96; // Depends on sampling rate 
    Vec3f angles = alpha * filteredGyro + (1.0 - alpha) * accelAngle;
    angles.z = filteredGyro.z;

    // Update the saved data with the latest values
    StoreState(tickStart, angles, unfilteredGyro);
}

IMU::IMU(u8 address)
    : m_address(address)
{
	WriteReg(MPU6050_PWR_MGMT_1, 0); // SLEEP bit enabled by default 
	Calibrate();
	StoreState(millis(), {}, {});
}

// The sensor should be motionless on a horizontal surface 
void IMU::Calibrate()
{
    IMUPack data;

    // Discard the first set of values read from the IMU
    GetRawData((u8*)&data);

    // Read and average the raw values from the IMU
    Vec3f acc;
    Vec3f gyr;
    const int numReadings = 10;
    for (int i = 0; i < numReadings; i++)
    {
        GetRawData((u8 *)&data);
        Vec3f newAcc(data.value.x_accel, data.value.y_accel, data.value.z_accel);
        acc += newAcc; 
        Vec3f newGyr(data.value.x_gyro, data.value.y_gyro, data.value.z_gyro);
        gyr += newGyr;
        delay(100);
    }
    
    // Store the raw calibration values globally
    baseAcc = acc / numReadings;
    baseGyr = gyr / numReadings;
}

int IMU::GetRawData(u8* data)
{
    // Read 14 bytes at once, containing acceleration, temperature and gyro.
    // With the default settings of the MPU-6050, there is no filter enabled, and the values are not very stable.  Returns the error value
    IMUPack* pack = (IMUPack*)data;

    int error = Read(MPU6050_ACCEL_XOUT_H, (u8*)pack, sizeof(*pack));

    // Swap all high and low bytes.
    // After this, the registers values are swapped, so the structure name like x_accel_l does no longer contain the lower byte.
    u8 swap;
#define SWAP(x,y) swap = x; x = y; y = swap

    SWAP((*pack).reg.x_accel_h, (*pack).reg.x_accel_l);
    SWAP((*pack).reg.y_accel_h, (*pack).reg.y_accel_l);
    SWAP((*pack).reg.z_accel_h, (*pack).reg.z_accel_l);
    SWAP((*pack).reg.t_h, (*pack).reg.t_l);
    SWAP((*pack).reg.x_gyro_h, (*pack).reg.x_gyro_l);
    SWAP((*pack).reg.y_gyro_h, (*pack).reg.y_gyro_l);
    SWAP((*pack).reg.z_gyro_h, (*pack).reg.z_gyro_l);

    return error;
}

int IMU::Read(int start, u8* buffer, int size)
{
    Wire.beginTransmission(m_address);

    if (Wire.write(start) != 1)
        return -10;

    int error = Wire.endTransmission(false);
    if (error != 0)
        return error;

    Wire.requestFrom(m_address, size, true);
    int i = 0;
    while (Wire.available() && i < size)
        buffer[i++] = Wire.read();
    if (i != size)
        return -11;

    return 0;
}

int IMU::Write(int start, const u8* data, int size)
{
    Wire.beginTransmission(m_address);
    if (Wire.write(start) != 1)
        return -20;

    if (Wire.write(data, size) != size)
        return -21;

    return Wire.endTransmission(true);
}

int IMU::WriteReg(int reg, u8 data)
{
    return Write(reg, &data, 1);
}
