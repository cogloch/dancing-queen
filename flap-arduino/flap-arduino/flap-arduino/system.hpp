#pragma once


// Interface for "systems" that update on every tick
class System
{
public:
    virtual ~System() = default;
    virtual void Tick() = 0;
};
