#pragma once
#include <Wire.h>
#include "imu_registers.hpp"
#include "common.hpp"


union IMUPack
{
	struct
	{
		u8 x_accel_h;
		u8 x_accel_l;
		u8 y_accel_h;
		u8 y_accel_l;
		u8 z_accel_h;
		u8 z_accel_l;
		u8 t_h;
		u8 t_l;
		u8 x_gyro_h;
		u8 x_gyro_l;
		u8 y_gyro_h;
		u8 y_gyro_l;
		u8 z_gyro_h;
		u8 z_gyro_l;
	} reg;
	struct
	{
		int x_accel;
		int y_accel;
		int z_accel;
		int temperature;
		int x_gyro;
		int y_gyro;
		int z_gyro;
	} value;
};

struct IMU
{
	u8 m_address;

	// State
	unsigned long last_read_time;
	Vec3f lastAngles; // Filtered angles
	Vec3f lastGyroAngles; // Store gyro angles to compare drift 

	// Calibration
	Vec3f baseAcc;
	Vec3f baseGyr;

    IMU() = default;
	IMU(u8 address);
	void Tick();
	void StoreState(unsigned long time, const Vec3f& angles, const Vec3f& gyroAngles);

	// The sensor should be motionless on a horizontal surface 
	void Calibrate();
    
	int GetRawData(u8* data);
	int Read(int start, u8* buffer, int size);
	int Write(int start, const u8* data, int size);
	int WriteReg(int reg, u8 data);
};