#pragma once
#include <Arduino.h>


class Logger
{
    // Only one instance allowed at any one time
    static bool s_instantiated;

public:
    Logger();
    ~Logger();
    
    template<typename T> 
    void Info(T msg)
    {
        Serial.print("INFO["); Serial.print(millis()); Serial.print("]:: ");
        Serial.println(msg);
    }
    
    template<typename T>
    void Error(T msg)
    {
        Serial.print("ERROR["); Serial.print(millis()); Serial.print("]:: ");
        Serial.println(msg);
    }
};