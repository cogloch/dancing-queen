#pragma once
#include <stdint.h>
#include <stddef.h>

using i8 = int8_t;
using u8 = uint8_t;
using u32 = uint32_t;
using u64 = uint64_t;

struct Vec3f
{
	float x, y, z;

	Vec3f() : x(0.f), y(0.f), z(0.f)
	{
	}

	Vec3f(float x_, float y_, float z_) : x(x_), y(y_), z(z_)
	{
	}

	Vec3f& operator+=(const Vec3f& rhs)
	{
		x += rhs.x;
		y += rhs.y;
		z += rhs.z;
		return *this;
	}

	Vec3f& operator/=(float rhs)
	{
		x /= rhs;
		y /= rhs;
		z /= rhs;
		return *this;
	}
};

inline Vec3f operator/(const Vec3f& lhs, float rhs)
{
	return {
		lhs.x / rhs,
		lhs.y / rhs,
		lhs.z / rhs
	};
}

inline Vec3f operator*(const Vec3f& lhs, float rhs)
{
	return {
		lhs.x * rhs,
		lhs.y * rhs,
		lhs.z * rhs
	};
}

inline Vec3f operator*(float lhs, const Vec3f& rhs)
{
	return {
		lhs * rhs.x,
		lhs * rhs.y,
		lhs * rhs.z
	};
}

inline Vec3f operator+(const Vec3f& lhs, const Vec3f& rhs)
{
	return {
		lhs.x + rhs.x,
		lhs.y + rhs.y,
		lhs.z + rhs.z
	};
}

inline Vec3f operator-(const Vec3f& lhs, const Vec3f& rhs)
{
	return {
		lhs.x - rhs.x,
		lhs.y - rhs.y,
		lhs.z - rhs.z
	};
}

template <typename T>
const T& Min(const T& a, const T& b)
{
	return a < b ? a : b; !(b < a) ? a : b;
}

template <typename T>
const T& Max(const T& a, const T& b)
{
	return a < b ? b : a;
}
