﻿#include <Arduino.h>  // Serial IO, timing
#include <Wire.h>     // I2C

#include "logger.hpp"
#include "sensing.hpp"
#include "actuation.hpp"
#include "ui_feedback.hpp"
#include "input_manager.hpp"
#include "user_interface.hpp"


int main()
{
    /*arduino::*/ init();   
    Wire.begin();
    
    // Shared between different systems and ui solely for info on their status 
    ActuationFeedback actFeedback;
    SensingFeedback  sensFeedback;
    
    Logger logger;
    InputManager inputMgr;
    Sensing sensing(&sensFeedback);
    UserInterface ui(&sensing, &inputMgr, &actFeedback, &sensFeedback);
    Actuation actuation(&sensing, ui.GetActuationCmdRef(), &actFeedback);

    System* const systems[] = {
      dynamic_cast<System*>(&inputMgr),
      dynamic_cast<System*>(&sensing),
      dynamic_cast<System*>(&ui), 
      dynamic_cast<System*>(&actuation)
    };
    const int numSystems = 4;

    for (;;)
    {
        for (int i = 0; i < numSystems; ++i)
            systems[i]->Tick();
        
        if (serialEventRun) serialEventRun();
    }
}