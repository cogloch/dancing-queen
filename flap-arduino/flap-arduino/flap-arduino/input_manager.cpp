#include "input_manager.hpp"
#include <assert.h>


bool InputManager::s_instantiated = false;

const u8 ROWS = 4;
const u8 COLS = 4;
const char keys[ROWS][COLS] = {
    {char(Keys::KEY_7), char(Keys::KEY_8), char(Keys::KEY_9), char(Keys::RIGHT)},
    {char(Keys::KEY_4), char(Keys::KEY_5), char(Keys::KEY_6), char(Keys::LEFT)},
    {char(Keys::KEY_1), char(Keys::KEY_2), char(Keys::KEY_3), char(Keys::TYPE)},
    {char(Keys::CLEAR), char(Keys::KEY_0), char(Keys::ENTER), char(Keys::DOT)}
};
const u8 rowPins[ROWS] = { A2, A3, A4, A5 };
const u8 colPins[COLS] = { A1, 10, 8, 9 };

InputManager::InputManager()
{
    assert(!s_instantiated);
    s_instantiated = true;
    
    m_keypad = Keypad(makeKeymap(keys), rowPins, colPins, ROWS, COLS);
    m_keyDown = NONE;
    m_keyDownTimer = millis();
    m_lastKeyUp = NONE;
    m_poppedUp = true;
}

InputManager::~InputManager()
{
    s_instantiated = false;   
}

void InputManager::Tick()
{
    char key = m_keypad.getKey();
    if (key)
    {
        m_poppedUp = false;
        m_lastKeyUp = key;
    }
        
    m_keyDown = NONE;
    if (m_keypad.getState() == HOLD)
    {   
        if (millis() - m_keyDownTimer > s_keyDownTimerLimit)
        {
            m_keyDownTimer = millis();
            m_keyDown = m_lastKeyUp;
        }
    }
}

Keys InputManager::GetKeyDown() const
{
    return m_keyDown;
}

Keys InputManager::GetLastKeyUp()
{
    if (m_poppedUp) return NONE;
    m_poppedUp = true;
    return m_lastKeyUp;
}