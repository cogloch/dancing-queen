#pragma once
#include "system.hpp"
#include "common.hpp"
#include "ui_feedback.hpp"


enum class MotorState : u8
{
    IDLE,
    MOVE_CW,
    MOVE_CCW
};

// Hardcoded to a single physical configuration:
//      Motor driver 
//             DIR    -------> 3 
//             STEP   -------> 4
//             ENABLE -------> 6

// Dependencies 
class Sensing;
class ActuationCmd; // UserInterface

class Actuation : public System
{
    // Only one instance allowed at any one time
    static bool s_instantiated; 
    
public:    
    Actuation(const Sensing* const, const ActuationCmd* const, ActuationFeedback* const);
    ~Actuation();
    Actuation(const Actuation&) = delete;
    Actuation& operator=(const Actuation&) = delete;

    void Tick() override;
    
private:
    const Sensing*      const m_sensing;
    const ActuationCmd* const m_command;
    ActuationFeedback*  const m_uiFeedback;
    
    const float m_errorBound = 2.f;
    
    MotorState m_motorState;
    
    //void Calibrate();
};