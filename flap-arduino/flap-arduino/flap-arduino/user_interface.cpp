#include "user_interface.hpp"
#include <float.h>
#include <assert.h>
#include "sensing.hpp"
#include "input_manager.hpp"


ActuationCmd::ActuationCmd() : m_targetAngle(-1000.f)
{
}

float ActuationCmd::GetTargetAngle() const
{
    return m_targetAngle;   
}

bool UserInterface::s_instantiated = false;

const char DEG_SYMBOL = (char)0b10110010;


UserInterface::UserInterface(const Sensing* const sensingRef, const InputManager* const inputMgrRef, const ActuationFeedback* const actuationFeedback, const SensingFeedback* const sensingFeedback)
    : m_sensing(sensingRef)
    , m_inputMgr(inputMgrRef)
    , m_actFeedback(actuationFeedback)
    , m_senseFeedback(sensingFeedback)
    , m_dt(0), m_lastTick(0)
{
    assert(!s_instantiated);
    s_instantiated = true;
    
    assert(m_inputMgr);
    
    m_display = Adafruit_LiquidCrystal(0);
    m_display.begin(s_numCols, s_numRows);
    
    m_refereshScreen = true;
}

UserInterface::~UserInterface()
{
    s_instantiated = false;
}

int UserInterface::FindPreset(float angle)
{
    for (int i = 0; i < int(FlapPreset::NONE); ++i)
        if (fabs(angle - presetAngles[i]) < FLT_EPSILON)
            return i;

    return int(FlapPreset::NONE);
}

void UserInterface::ClearInput()
{
    if (inputType == InputType::PRESET)
        m_inputPreset = 0;
    else
        m_inputAngle = 0.f;
}

void UserInterface::CommitCommand()
{
    if (inputType == InputType::VALUE)
        m_actuationCmd.m_targetAngle = m_inputAngle;
    else
        m_actuationCmd.m_targetAngle = presetAngles[m_inputPreset];
}

void UserInterface::SwitchType()
{
    inputType = inputType == InputType::PRESET ? InputType::VALUE : InputType::PRESET;
}

void UserInterface::OnKeyUpPreset(Keys key)
{
    switch (key)
    {
        case Keys::LEFT:
            m_inputPreset--;
            if (m_inputPreset < 0)
                m_inputPreset = int(FlapPreset::NONE) - 1;
            break;
        case Keys::RIGHT:
            m_inputPreset++;
            if (m_inputPreset == int(FlapPreset::NONE))
                m_inputPreset = 0;
            break;
        default:
            if (key > 9) break;
            int idx = key - '0';
            if (idx >= 0 && idx < int(FlapPreset::NONE))
                m_inputPreset = idx;
            break;
    }
}

void UserInterface::OnKeyUpValue(Keys key)
{
    switch (key)
    {
        case Keys::LEFT:
            m_inputAngle -= .1f;
            m_inputAngle = Max(m_inputAngle, presetAngles[int(FlapPreset::MIN)]);
            break;
        case Keys::RIGHT:
            m_inputAngle += .1f;
            m_inputAngle = Min(m_inputAngle, presetAngles[int(FlapPreset::MAX)]);
            break;
        default:
            if (key > 9) break;
            m_inputAngle = int(key);
            break;
    }
}

void UserInterface::OnKeyHoldValue(Keys key, u32 dt)
{
    switch (key)
    {
        case Keys::LEFT:
            m_inputAngle -= .01f * dt;
            m_inputAngle = Max(m_inputAngle, presetAngles[int(FlapPreset::MIN)]);
            break;
        case Keys::RIGHT:
            m_inputAngle += .01f * dt;
            m_inputAngle = Min(m_inputAngle, presetAngles[int(FlapPreset::MAX)]);
            break;
    }
}

void UserInterface::Tick()
{
    m_dt = millis() - m_lastTick;
    m_lastTick = millis();
    
    switch (m_curScreen)
    {
        case Screen::INIT:
            TickInit();
            break;
        case Screen::OPERATION:
            TickOperation();
            break;
    }
}

void UserInterface::TickInit()
{
    // Screen 1: Initializing
    //                -> Line 2: Step
    //                        -> Module connection
    //                                 -> UART
    //                                 -> RF 
    //                        -> Sensing
    //                                 -> Connection
    //                                 -> Initial calibration
    //                        -> Actuation
    //                                 -> Connection
    //                                 -> Initial mechanism test
    //                -> Line 3: Progress
}

void UserInterface::TickOperation()
{
    // Screen 2: Operation
    //                -> Line 1: Status
    //                        -> Holding
    //                        -> Moving towards <target angle> (optional<preset>)
    //                -> Line 2: Flap at <angle> (optional<preset>)
    //                -> Line 3: Current input type
    //                        -> Specific angle
    //                        -> Preset
    //                -> Line 4: Current input
    enum ScreenLine
    {
        STATUS,
        FLAP_POS,
        INPUT_TYPE,
        CUR_INPUT
    };
    bool needsUpdating[4] = {};
        
    auto pressedKey = m_inputMgr->GetLastKeyUp();
    if (pressedKey)
    {
        needsUpdating[ScreenLine::CUR_INPUT] = true;
    
        switch (pressedKey)
        {
            case Keys::CLEAR:
                ClearInput();
                break;
            case Keys::ENTER:
                CommitCommand();
                break;
            case Keys::TYPE:
                SwitchType();
                needsUpdating[INPUT_TYPE] = true;
                break;
            default:
                if (inputType == InputType::VALUE)
                    OnKeyUpValue(pressedKey);
                else
                    OnKeyUpPreset(pressedKey);
                break;
        }
    }         
    
    auto holdKey = m_inputMgr->GetKeyDown();
    if (holdKey)
    {
        needsUpdating[ScreenLine::CUR_INPUT] = true;
        
        if (inputType == InputType::VALUE)
            OnKeyHoldValue(holdKey, m_dt);
    }
    
    const float actTarget = m_actFeedback->GetTarget();
    enum ActStatus
    {
        HOLDING,
        MOVING
    } actStatus = HOLDING;
    if (actTarget > -100.f)
        actStatus = MOVING;
    
    static float lastActTarget = -100.f;
    if (fabs(lastActTarget - actTarget) > FLT_EPSILON)
    {
        needsUpdating[ScreenLine::STATUS] = true;
        lastActTarget = actTarget;
    }
    
    static u32 lastPresetStateChange = 0;
    enum FlapStateDisplay
    {
        ANGLE,
        PRESET
    };
    static FlapStateDisplay movingToDisplayType = ANGLE;
    if (actStatus == ActStatus::MOVING && (millis() - lastPresetStateChange > 3000))
    {
        lastPresetStateChange = millis();
        movingToDisplayType = movingToDisplayType == ANGLE ? PRESET : ANGLE;
        needsUpdating[ScreenLine::STATUS] = true;

        auto preset = FindPreset(actTarget);
        if (preset == int(FlapPreset::NONE) && movingToDisplayType == PRESET)
        {
            movingToDisplayType = ANGLE;
            needsUpdating[ScreenLine::STATUS] = false;
        }
    }

    if (needsUpdating[ScreenLine::STATUS] || m_refereshScreen)
    {
        ClearLine(0);
        m_display.setCursor(0, 0);
        switch (actStatus)
        {
            case ActStatus::HOLDING:
                m_display.print("Holding");
                break;
            case ActStatus::MOVING:
                m_display.print("Moving to ");

                if (movingToDisplayType == ANGLE)
                {
                    m_display.print(actTarget, 1);
                    m_display.print(DEG_SYMBOL);
                }
                else
                {
                    auto preset = FindPreset(actTarget);
                    m_display.print(flapPresetNames[preset]);
                }
                break;
        }
    }

    static float lastDefl = -100.f;
    const float flapDeflection = m_senseFeedback->GetDeflectionEstimate();
    if (fabs(flapDeflection - lastDefl) > .3f /*FLT_EPSILON*/)
    {
        lastDefl = flapDeflection;
        needsUpdating[ScreenLine::FLAP_POS] = true;
    }

    static FlapStateDisplay posDisplayType = FlapStateDisplay::ANGLE;

    static u32 lastPosDisplayChange = 0;
    auto posPreset = FindPreset(flapDeflection);
    if (posPreset == int(FlapPreset::NONE))
    {
        if (posDisplayType == FlapStateDisplay::PRESET)
        needsUpdating[ScreenLine::FLAP_POS] = true;
        posDisplayType = FlapStateDisplay::ANGLE;
    }
    else
    {
        if (millis() -  lastPosDisplayChange > 3000)
        {
            lastPosDisplayChange = millis();
            posDisplayType = posDisplayType == ANGLE ? PRESET : ANGLE;
            needsUpdating[ScreenLine::FLAP_POS] = true;
        }
    }

    if (needsUpdating[ScreenLine::FLAP_POS] || m_refereshScreen)
    {
        ClearLine(1, sizeof("Flap at ") - 1);
        m_display.setCursor(0, 1);
        m_display.print("Flap at ");

        if (posDisplayType == ANGLE)
        {
            m_display.print(flapDeflection, 1);
            m_display.print(DEG_SYMBOL);
        }
        else
        {
            auto preset = FindPreset(flapDeflection);
            m_display.print(flapPresetNames[preset]);
        }
    }

    if (needsUpdating[ScreenLine::INPUT_TYPE] || m_refereshScreen)
    {
        ClearLine(2, sizeof("Choose ") - 1);
        m_display.setCursor(0, 2);

        switch (inputType)
        {
        case InputType::PRESET:
            m_display.print("Choose preset:");
            break;
        case InputType::VALUE:
            m_display.print("Choose flap angle:");
            break;
        }
    }

    if (needsUpdating[ScreenLine::CUR_INPUT] || m_refereshScreen)
    {
        ClearLine(3);
        m_display.setCursor(0, 3);

        switch (inputType)
        {
        case InputType::PRESET:
            m_display.print(m_inputPreset);
            m_display.print(": ");
            m_display.print(flapPresetNames[m_inputPreset]);
            m_display.print(" (");
            m_display.print(presetAngles[m_inputPreset], 1);
            m_display.print(DEG_SYMBOL);
            m_display.print(")");
            break;
        case InputType::VALUE:
            m_display.print(m_inputAngle, 1);
            m_display.print(DEG_SYMBOL);
            auto preset = FindPreset(m_inputAngle);
            if (preset != int(FlapPreset::NONE))
            {
                m_display.print(" (");
                m_display.print(flapPresetNames[preset]);
                m_display.print(")");
            }
            break;
        }
    }

    m_refereshScreen = false;
}

const ActuationCmd* const UserInterface::GetActuationCmdRef() const
{
    return &m_actuationCmd;
}

void UserInterface::ClearLine(int line, int offset)
{
    assert(line >= 0 && line < s_numRows);
    assert(offset >= 0 && offset < s_numCols);
    
    // TODO make this nicer
    static const char clearLine[s_numCols];
    static bool init = false;
    if (!init)
    {
        memset(clearLine, ' ', s_numCols);
        memset(clearLine + s_numCols - 1, '\0', 1);
        init = true;
    }
    
    m_display.setCursor(offset, line);
    m_display.print(clearLine + offset);
}
