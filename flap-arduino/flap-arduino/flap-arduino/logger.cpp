#include "logger.hpp"
#include <assert.h>


bool Logger::s_instantiated = false;

Logger::Logger()
{
    assert(!s_instantiated);
    s_instantiated = true;
    
    Serial.begin(9600);
}

Logger::~Logger()
{
    s_instantiated = false;
}