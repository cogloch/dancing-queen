#pragma once
#include "common.hpp"


// TODO separate, keep comms.h independent of everything else 
enum class MsgType : u8
{
	TO_REMOTE_CUR_ANGLE,
	TO_REMOTE_CUR_ACTION,

	TO_FLAP_ANGLE_CMD,

	NONE
};

const u8 msgSizes[] = {
	4,   // TO_REMOTE_CUR_ANGLE
	1,   // TO_REMOTE_CUR_ACTION
	4    // TO_FLAP_ANGLE_CMD
};

using BufferId = i8;
const BufferId INVALID_BUFFER_ID = -1;

class Comms
{
public:
	virtual ~Comms() {}
	virtual BufferId Send(const void* buffer, u8 bufferSz, u8 target, bool requestACK, MsgType type, bool overwrite) = 0;
	virtual void Tick() = 0;
};

class RFM69;

// TODO error handling 
class CommsRF : public Comms
{
public:
	// @encryptKey must be 16 bytes
	CommsRF(u8 node, u8 network, const char* encryptKey = nullptr);
	~CommsRF();
	BufferId Send(const void* buffer, u8 bufferSz, u8 targetNode, bool requestACK, MsgType type = MsgType::NONE, bool overwrite = true) override;
	void Tick() override;

	u8 m_data[61]; // Copy last received RF buffer each time 
	u8 m_dataSz;

	// TODO allow removal of send nodes from outside 

private:
	RFM69* m_radio;

	struct Buffer
	{
		Buffer();
		~Buffer();
		u8* data;
		u8  dataSz;
		
		// TODO temp
		MsgType type; 

		BufferId id;
		static BufferId s_nextId;

		u8 targetNode;
		u8 retries;

		Buffer* next;
	};

	Buffer* m_sendList; // TODO: memory pool
	
	// Returns the next node after the removed one (or none)
	Buffer* RemoveSendBuffer(BufferId id);
};

class CommsI2C : public Comms
{
public:
	CommsI2C() = delete;
	void Tick() override {}
	BufferId Send(const void* buffer, u8 bufferSz, u8 target, bool requestACK, MsgType type, bool overwrite) override { return INVALID_BUFFER_ID; }
};

class CommsUART : public Comms
{
public:
	CommsUART() = delete;
	void Tick() override;
	BufferId Send(const void* buffer, u8 bufferSz, u8 target, bool requestACK, MsgType type, bool overwrite) override;
};
