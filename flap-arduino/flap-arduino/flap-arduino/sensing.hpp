
#define SAFE_SENSE


#pragma once
#include "system.hpp"
#include "imu.hpp"

// Dependencies 
#include "ui_feedback.hpp"


struct NavHistory
{
	static const int maxEstimates = 20;
	//size_t numEstimtes = 0; // Ring; there are always <maxEstimates> estimates 
	float estimates[maxEstimates];

	NavHistory()
	{
		memset(this, 0, sizeof(NavHistory));
	}

	void AddEstimate(float newEstimate)
	{
		// TODO temp
		// Shift whole array to the left; last estimate index alaways == maxEstimates
		for (int i = 1; i < maxEstimates; ++i)
		{
			estimates[i - 1] = estimates[i];
		}
		estimates[maxEstimates - 1] = newEstimate;
		/*
		if (numEstimtes == maxEstimates)
		numEstimtes = 0;

		estimates[numEstimtes++] = newEstimate;*/
	}
};

// Hardcoded to a single physical configuration:
//         Moving    IMU: AD0 pin LOW     (I2C addr 0x68)
//         Reference IMU: AD0 pin HIGH    (I2C addr 0x69)


// temp
#ifdef SAFE_SENSE
const float potMin = 10.f; // +30deg
const float potMax = 837.f; // -10deg
#endif

class Sensing : public System
{
    // Only one instance allowed at any one time
    static bool s_instantiated;
    
public:
    Sensing(SensingFeedback* const);
    ~Sensing();
    Sensing(const Sensing&) = delete;
    Sensing& operator=(const Sensing&) = delete;
    
    void Tick() override;
    
    bool IsStable() const;
    float GetEstimatedDeflection() const;
    
    enum class State
    {
        CALIBRATE_SENSOR,
        CALIBRATE_MECHANISM,
        OPERATIONAL,
        FAILED    
    };
    State GetState();
    
private:
    State m_state;

    IMU m_referenceImu;
    IMU m_movingImu;
    
    SensingFeedback* const m_uiFeedback;
    float m_estimateDefl;

    NavHistory m_history;
    const float m_stableBound = 2.f;
    
    float m_potVal;
    
    void CalibrateMechanism();
};