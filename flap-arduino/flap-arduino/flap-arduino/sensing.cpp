#include "sensing.hpp"
#include <assert.h>
#include <math.h>
#include <Arduino.h>


// TEMP 
//                  POT RANGES
struct PotRange
{
    float minReal;
    float maxReal;
    
    int minPot;
    int maxPot;
};

PotRange ranges[10];

//                IMU CALIBRATION BASED ON POT 
enum MechanismLimitType
{
    LIMIT_LOW,
    LIMIT_HIGH
};

// Axis about which deflecting the flap would give a change in estimate
enum ImuOrientation
{
    AXIS_X,
    AXIS_Y,
    AXIS_Z
} imuOrientation;

struct MechanismLimit
{
    enum MotorDir
    {
        CW, CCW
    } motorDir;
    float value;
};

                          // Use mapping of [-10, +30]->[LIMIT_LOW.value, LIMIT_HIGH.value] to get [desired deflection] -> [estimated deflection (needed)]
MechanismLimit limits[2]; // [0] = lower limit (corresponding to -10)
                          // [1] = upper limit (corresponding to +30)


bool Sensing::s_instantiated = false;

Sensing::Sensing(SensingFeedback* const uiFeedback)
    : m_uiFeedback(uiFeedback)
    , m_estimateDefl(-100.f)
{
    assert(!s_instantiated);
    s_instantiated = true;
    
    // Sensor calibration (m_state == CALIBRATE_SENSOR) in ctor 
    m_movingImu = IMU(0x68);
    m_referenceImu = IMU(0x69);
    
    m_state = State::CALIBRATE_MECHANISM;
    
    #ifdef SAFE_SENSE
    ranges[0] = { -16.f, -10.f,  85, 100 };
    ranges[1] = { -10.f,  -5.f, 100, 125 };
    ranges[2] = {  -5.f,   0.f, 125, 150 };
    ranges[3] = {   0.f,   5.f, 150, 300 };
    ranges[4] = {   5.f,  10.f, 300, 550 };
    ranges[5] = {  10.f,  15.f, 550, 690 };
    ranges[6] = {  15.f,  20.f, 690, 740 };
    ranges[7] = {  20.f,  25.f, 740, 800 };
    ranges[8] = {  25.f,  30.f, 800, 870 };
    ranges[9] = {  30.f,  36.f, 870, 920 };  
     
    const float offset = 65;   
    for (int i = 0; i < 10; ++i)
    {
        ranges[i].minPot -= offset;
        ranges[i].maxPot -= offset;
    }  
    #endif
}

Sensing::~Sensing()
{
    s_instantiated = false;
}

void Sensing::CalibrateMechanism()
{
    // Move one way until moving further doesn't change the sensing system's estimate for a few seconds - that's one of the limits.
    // Switch direction.
    // Move the other way, do the same, get the other limit.

    m_state = State::OPERATIONAL;
}

void Sensing::Tick()
{
    if (m_state == State::CALIBRATE_MECHANISM)
    {
        //m_state
    }
    
    #ifdef SAFE_SENSE
    m_potVal = analogRead(A0);
    #endif
    
    m_referenceImu.Tick();
    m_movingImu.Tick();
    
    m_estimateDefl = -(m_referenceImu.lastAngles - m_movingImu.lastAngles).x;
    #ifdef SAFE_SENSE
    //m_estimateDefl = map(m_potVal, potMin, potMax, -10, 30);
    // Don't lerp because the rail is not linear 
    for (int i = 0; i < 10; ++i)
    {
        if (m_potVal > ranges[i].minPot && m_potVal < ranges[i].maxPot)
        {
            m_estimateDefl = ranges[i].minReal + (ranges[i].maxReal - ranges[i].minReal) / (ranges[i].maxPot - ranges[i].minPot) * (m_potVal - ranges[i].minPot);
            break;    
        }        
    }
    #endif
    //Serial.println(m_estimateDefl);
    m_history.AddEstimate(m_estimateDefl);
    if (IsStable())
        m_uiFeedback->m_deflection = m_estimateDefl;
}

bool Sensing::IsStable() const
{
    #ifdef SAFE_SENSE
    return true; 
    #endif
    
    // Compare last nav estimate with the past <x> (e.g. 5)
    // If any delta > <gradient calculted along the way> (e.g. 1 deg), then assume readings are not in a stable state
    for (size_t i = 0; i < NavHistory::maxEstimates - 1; ++i)
        if (fabs(m_history.estimates[i] - m_history.estimates[NavHistory::maxEstimates - 1]) > m_stableBound)
            return false;

    return true;   
}

float Sensing::GetEstimatedDeflection() const
{
    return m_estimateDefl;
}