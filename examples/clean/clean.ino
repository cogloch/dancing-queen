#include "motor.hpp"
#include "imu.hpp"
#include "comms.h"

Motor spinny;
IMU* m_imu;
IMU* m_imuFixed;
Comms* comms;

void setup()
{
    Serial.begin(9600);
    Wire.begin();

	m_imu = new IMU(0x68);
	m_imuFixed = new IMU(0x69);

    comms = new CommsRF(2, 0, "verynicepwd");

    // Init motor
	pinMode(DIR_A, OUTPUT); // CH A -- HIGH = forwards and LOW = backwards???
	pinMode(DIR_B, OUTPUT); // CH B -- HIGH = forwards and LOW = backwards???
	pinMode(9, OUTPUT); // brake (disable) CH A
	pinMode(8, OUTPUT); // brake (disable) CH B 
}

float m_target = 0.f;

void loop()
{
    m_imu->Tick();
	m_imuFixed->Tick();

	// Is action needed? 
	const float estDefl = (m_imuFixed->lastAngles - m_imu->lastAngles).x;
	Serial.print("Estimated deflection: "); Serial.print(estDefl); 
	Serial.print(" | Estimated guidance error: "); Serial.print(fabs(estDefl - m_target));
	if (spinny.state == Motor::IDLE) Serial.println(" HOLDING");
	else                            Serial.println(" MOVING");

    spinny.Tick();
}